# HTTP Drills I

The http server is created at the port 3000 on the localhost and tested each test using the postman API platform.

> The main javaScript file is named script.js

The output of each GET request is represented in following screenshots.

## For GET /html

The html file passed is index.html

![](Images/htmlRequest.png)

## For GET /json

The json file passed is string.json

![](Images/jsonRequest.png)

## For GET /uuid

> npm install uuid 

uuid is required to generate random uuids.

![](Images/uuidRequest.png)

## For GET /status/{status_code} 

![](Images/statusRequest.png)

## For GET /delay/{delay_in_seconds}

![](Images/delayRequest.png)
