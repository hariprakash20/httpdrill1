const fs = require('fs');
const http = require('http');
const uuid = require('uuid');
const port = 3000;

const server = http.createServer((request,response)=>{
    if(request.url==='/html'){
        fs.readFile('index.html','utf-8', (err,data) => {
            if(err){
                response.writeHead(404);
                response.write('file not found');
            }else{
                response.writeHead(200,{'Content-Type':'text/html'})
                response.write(data);
            }
            response.end()
        })
    }
    if(request.url === '/json'){
        fs.readFile('string.json','utf-8',(err,data)=>{
            if(err){
                response.writeHead(404);
                response.write('file not found');
            }else{
                response.writeHead(200,{'Content-Type':'application/json'});
                response.write(data);
            }
            response.end();
        })
    }
    if(request.url === '/uuid'){
        response.writeHead(200,{'Content-Type':'application/json'})
        response.write(JSON.stringify(`{ uuid : ${uuid.v4()} }`));
        response.end();
    }
    if(request.url.includes('/status/')){
        let statusCode = request.url.replace('/status/','');
        if(http.STATUS_CODES[statusCode]){
            response.writeHead(statusCode,{'Content-Type':'application/json'});
            response.write(JSON.stringify(`{${statusCode} : ${http.STATUS_CODES[statusCode]}} `))
        }
        else{
            response.writeHead(404);
            response.write('No such code exist');
        }
        response.end();
    }
    if(request.url.includes('/delay/')){
        let delay = +(request.url.replace('/delay/',''));
        if(Number.isInteger(delay)){
            setTimeout(()=>{
                response.writeHead(200,{'Content-Type':'text/html'})
                response.write('success');
                response.end(); 
            },delay*1000);
        }
        else{
            response.writeHead(404)
            response.write('delay is not a number');
            response.end();
        }
        
    }
})

server.listen(port, (err)=>{
    if(err) console.log(err);
    else console.log('server is listening on '+ port);
})